<?php

use yii\db\Migration;
use yii\db\Schema;

class m170521_130906_create_table_customers extends Migration
{
    public function up()
    {
        $this->createTable('customers', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING,
        ]);
    }

    public function down()
    {
        echo "m170521_130906_create_table_customers cannot be reverted.\n";

        $this->dropTable('customers');
        
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
